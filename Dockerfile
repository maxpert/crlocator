FROM alpine:latest

RUN mkdir -p /app
ENV MMDB_KEY ""
COPY crlocator-static /usr/bin/crlocator
CMD crlocator start
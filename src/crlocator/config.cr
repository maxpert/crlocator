module Crlocator
  class Config
    property port, db_path, db_url, update_interval, loaderio_id, mmdb_key

    def initialize(
      @port : Int32, 
      @db_path : String, 
      @db_url : String, 
      @update_interval : Int32, 
      @mmdb_key : String?,
      @loaderio_id : String?)
    end
  end
end
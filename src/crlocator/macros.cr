macro route_action(method, path, klass, name)
  {{method}} {{path}} { |ctx| Crlocator::Actions::{{klass}}.new(ctx).{{name}} }
end

macro global_config(name)
  Crlocator.config.{{name}}
end
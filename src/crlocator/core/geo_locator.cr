require "maxminddb"
require "json"
require "log"

module Crlocator::Core

  class Location
    include JSON::Serializable

    property city : String
    property country : String
    property latitude : Float64
    property longitude : Float64
    property timezone : String
    property ip : String

    def initialize(
        @city : String,
        @country : String,
        @latitude : Float64,
        @longitude : Float64,
        @timezone : String,
        @ip : String
      )
    end
  end

  class LocationError
    include JSON::Serializable

    property error : String

    def initialize(err : String|Nil)
      @error = err || "Unknown error"
    end
  end

  class GeoLocator
    @@global : GeoLocator|Nil = nil
    @@random_ips = [
      "173.104.131.25",
      "158.92.105.238",
      "233.249.206.14",
      "156.236.7.157",
      "228.218.28.17",
      "206.2.192.242",
      "131.175.61.215",
      "58.207.17.188",
      "199.181.202.23"
    ]

    def self.load!(path : String)
      Log.info { "Loading DB on path: #{path}" }
      db = GeoLocator.new(path)
      Log.debug { "Warming up DB..." }
      @@random_ips.each do |ip|
        db.find(ip)
      end
      @@global = db
    end

    def self.ready?
      @@global != nil
    end

    def self.instance
      @@global
    end

    def initialize(path : String)
      @db = MaxMindDB.open(path)
    end

    def find(ip : String, lang : String|Nil = nil)
      begin
        return lookup(ip, lang)
      rescue ex
        Log.error { ex.to_s }
        return LocationError.new(ex.message)
      end
    end

    private def lookup(ip : String, lang : String|Nil)
      lang ||= "en"
      result = @db.get(ip)
      return LocationError.new("Unable to find #{ip}") unless result["city"]? && result["city"]["names"][lang]?

      Location.new(
          result["city"]["names"][lang].as_s,
          result["country"]["names"][lang].as_s,
          result["location"]["latitude"].as_f,
          result["location"]["longitude"].as_f,
          result["location"]["time_zone"].as_s,
          ip
      )
    end
  end

  extend self

  def geo_locator
    GeoLocator.instance.not_nil!
  end
end

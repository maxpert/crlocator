require "http/client"
require "crystar"
require "log"

module Crlocator::Core::Updater
  extend self

  def download_gz(path : String, url : String)
    HTTP::Client.get(url) do |response|
      Compress::Gzip::Reader.open(response.body_io) do |gzip|
        Crystar::Reader.open(gzip) do |tar|
          tar.each_entry do |entry|
            if entry.name.ends_with?(".mmdb") 
              Log.info { "Extracting #{entry.name} to #{path}..." }
              File.write(path, entry.io)
            end
          end
        end
      end
    end

    true
  end

  def download_geo_db(db_path, db_url, update_existing)
    config = Crlocator.config
    
    if !update_existing && File.exists?(db_path)
      Log.debug { "Update file path #{db_path} already exists" }
      return
    end

    if config.mmdb_key.nil? && !File.exists?(db_path)
      Log.error { "Maxmind download key not configured, and existing #{db_path} is missing." }
      Log.error { "Either configure a download key or make sure #{db_path} exists! Exiting..." }
      exit(-1)
    end

    Log.warn { "Path missing #{db_path}..." }
    Log.info { "Downloading... #{db_url}..." }
    download_gz(db_path, db_url)
    Log.info { "Download complete..." }
  end

  def db_auto_updater
    config = Crlocator.config
    if config.mmdb_key.nil?
      Log.info { "Background update disabled because download key is not configured" }
      return
    end

    loop do
      sleep config.update_interval.days

      begin
        download_geo_db(config.db_path, config.db_url, true)
        GeoLocator.load!(config.db_path)
      rescue exception
        puts exception
      end
    end
  end

end

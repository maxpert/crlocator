module Crlocator
  class PublicStorage
    macro embed_file_content(path)
      {{ `cat #{__DIR__}/../../baked_root/#{path}`.stringify }}
    end

    @@content_map : Hash(String, String) = {
      "views/home.mustache" => embed_file_content("views/home.mustache")
    }

    def self.get_content(path : String)
      @@content_map[path]? || ""
    end
  end
end
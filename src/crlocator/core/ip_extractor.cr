require "http"

module Crlocator::Core::IPExtractor
  extend self

  @@potential_ip_headers = [
    "X_Client_Ip",
    "CF_Connecting_IP",
    "X_Forwarded_For",
    "Proxy_Client_IP",
    "WL_Proxy_Client_IP",
    "HTTP_CLIENT_IP",
    "HTTP_X_FORWARDED_FOR"
  ]

  def detect_ip(headers : HTTP::Headers)
    ip_headers = @@potential_ip_headers.map { |header|
      dashed_header = header.tr("_", "-")
      underscored_header = header.tr("-", "_")

      headers[underscored_header]? ||
      headers[dashed_header]? ||
      headers["HTTP_#{underscored_header}"]? ||
      headers["HTTP-#{dashed_header}"]?
    }.select { |v| v }

    return ip_headers.first.try &.split(",").first.strip if ip_headers.size > 0
    return nil
  end
end

require "kemal"
require "./config"
require "./macros"
require "./actions/base"
require "./actions/*"

module Crlocator::Router
  extend self

  def bootstrap
    route_action get, "/", Default, home
    route_action get, "/loaderio-#{global_config loaderio_id}/", Default, loader_io unless global_config(loaderio_id).nil?

    route_action get, "/json/:ip", API, locate
    route_action get, "/json", API, locate
  end
end


module Crlocator::Actions
  class Default < Base
    include Crlocator::Core
    include Crlocator::Core::IPExtractor

    def home
      ip = detect_ip(@context.request.headers) || "0.0.0.0"
      location = geo_locator.find(ip)

      case location
      when Crlocator::Core::Location
        city = location.city
        country = location.country
      when Crlocator::Core::LocationError
        error_message = location.error
      end

      render "home", {
        "host_with_port" => @context.request.host_with_port,
        "ip" => ip,
        "city" => city,
        "country" => country,
        "error" => error_message
      }
    end

    def loader_io
      "loaderio-#{global_config loaderio_id}"
    end
  end
end

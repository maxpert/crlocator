require "crustache"

module Crlocator::Actions
  class Base
    @@template_cache = Hash(String, Crustache::Syntax::Template).new()

    def initialize(@context : HTTP::Server::Context)
    end

    def render(name : String, data)
      path = "views/#{name}.mustache"
      template = @@template_cache[path]?
      template = Crustache.parse(Crlocator::PublicStorage.get_content(path)) if template.nil?
      Crustache.render(template, data)
    end

    def render(name : String)
      render(name, nil)
    end

    def render_json(response)
      @context.response.content_type = "application/json"
      response.to_json
    end

    def render_jsonp(serializable)
      response = serializable.to_json
      jsonp_callback = @context.request.query_params.fetch "callback", nil
      @context.response.content_type = "application/json"
      @context.response.content_type = "application/javascript" if jsonp_callback

      response = "#{jsonp_callback}(#{response});" if jsonp_callback
      response
    end
  end
end

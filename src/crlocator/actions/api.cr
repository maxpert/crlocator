module Crlocator::Actions
  class API < Base
    include Crlocator::Core
    include Crlocator::Core::IPExtractor

    def locate
      handle_locate(
        @context.params.url.fetch("ip", nil) ||
        detect_ip(@context.request.headers) ||
        "0.0.0.0"
      )
    end

    private def handle_locate(ip : String)
      render_jsonp geo_locator.find(ip)
    end
  end

end

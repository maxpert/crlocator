require "./crlocator"
require "option_parser"
require "log"
require "file"

db_download_key = ENV["MMDB_KEY"]?
db_path = "./GeoLite2-City.mmdb"
db_download_url = "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=#{db_download_key || ""}&suffix=tar.gz"
bind_port = 3000
auto_update_interval = 24
rest_args = Array(String).new()
logger = Log::IOBackend.new(STDOUT)
loaderio_id : String? = nil

optionParser = OptionParser.parse do |parser|
  parser.banner = "Usage: colocator [options] (start)"
  parser.on("-p PORT", "--port=PORT", "Port to start listening on (default: #{bind_port})") { |port|
    bind_port = port.to_i
  }
  parser.on("-d PATH", "--db=PATH", "Path to database (default: #{db_path})") { |path|
    db_path = path
  }
  parser.on("-t HOURS", "--update=HOURS", "Update DB interval hours (default: #{auto_update_interval})") { |hours|
    auto_update_interval = hours.to_i
  }
  parser.on("-l PATH", "--log=PATH", "Path to log fine (default: STDOUT)") { |path|
    logger = Log::IOBackend.new(File.open(path, "w"))
  }
  parser.on("-u URL", "--db-url=URL", "Download URL for database (default: #{db_download_url})") { |url|
    db_download_url = url
  }
  parser.on("-v", "--verbose", "Verbose logging") {
    Log.setup(:debug, logger)
  }
  parser.on("-x ID", "--loaderio ID", "Allow benchmarking via Loader.io where ID is the loaderio-{ID}") { |id|
    loaderio_id = id
  }
  parser.on("-h", "--help", "Show this help") {
    puts parser
    exit -1
  }
  parser.unknown_args { |args|
    rest_args = args
  }
end

if db_download_key.nil? 
  Log.warn { "Maxmind key not configured under environment variable MMDB_KEY" }
end

if rest_args.size < 1 || rest_args[0] != "start"
  puts(optionParser.to_s)
  exit -1
end

# Bang!
Crlocator.run(
  Crlocator::Config.new(bind_port, db_path, db_download_url, auto_update_interval, db_download_key, loaderio_id)
)

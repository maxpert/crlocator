require "file"
require "log"

require "./crlocator/core/*"
require "./crlocator/*"

module Crlocator
  class_property config = Config.new(
    3000, 
    "./", 
    "",
    24,
    nil,
    nil
  )

  def self.run(config : Config)
    @@config = config

    Core::Updater.download_geo_db(config.db_path, config.db_url, false)
    Core::GeoLocator.load!(config.db_path)
    spawn { Core::Updater.db_auto_updater }

    Router.bootstrap
    Log.info { "Starting server in #{config.port}" }
    Kemal.run(config.port)
  end
end

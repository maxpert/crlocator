docker run --rm -it -v $PWD:/app -w /app crystallang/crystal:0.35.1-alpine /bin/sh -c "shards install && crystal build src/main.cr --static --release --progress -o crlocator-static --error-trace"
docker build -t maxpert/crlocator:latest .
rm -rf crlocator-static

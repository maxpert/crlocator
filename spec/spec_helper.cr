require "spec"
require "logger"
require "../src/crlocator"

module SpecHelper
  extend self

  @@links = {
    country: "http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz",
    city: "http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz"
  }

  def get_db(db_link, filename) : String
    path = "spec/cache/#{filename.gsub(".gz", "")}"
    return path if File.exists?("spec/cache/#{filename.gsub(".gz", "")}")

    puts "Downloading db #{filename} to #{path}"
    Process.run "sh", {"-c", "curl #{db_link} -o spec/cache/#{filename}"}
    Process.run "sh", {"-c", "gunzip spec/cache/#{filename}"}

    File.delete("spec/cache/#{filename}") if File.exists?("spec/cache/#{filename}")
    return path
  end

  def get_city_db_path
    links = @@links
    self.get_db(links[:city], links[:city].split("/").last)
  end

  def get_country_db_path
    links = @@links
    self.get_db(links[:country], links[:country].split("/").last)
  end

  def geo_locator
    Crlocator.logger = Logger.new(nil)
    Crlocator::Core::GeoLocator.load!(get_city_db_path) if !Crlocator::Core::GeoLocator.ready?
    Crlocator::Core.geo_locator
  end

  def build_headers(key : String, ip : Array(String))
    headers = HTTP::Headers.new()
    headers[key] = ip
    headers
  end
end

require "../../spec_helper"

describe Crlocator::Core::GeoLocator do
  it "should fail to open invalid database" do
    expect_raises(ArgumentError) do
      Crlocator::Core::GeoLocator.new(SpecHelper.get_city_db_path + ".blah")
    end
  end

  it "should track valid IP address" do
    locator = SpecHelper.geo_locator
    locator.find("61.192.135.96").should be_truthy
  end

  it "should return error on invalid IP address" do
    locator = SpecHelper.geo_locator
    locator.find("127.0.0.1").should be_a(Crlocator::Core::LocationError)
  end

  it "should return serializable error on invalid IP" do
    locator = SpecHelper.geo_locator
    locator.find("127.0.0.1").to_json.should be_truthy
  end

  it "should return json serializable location" do
    locator = SpecHelper.geo_locator
    location = locator.find("61.192.135.96")
    location.to_json.should be_truthy
  end
end

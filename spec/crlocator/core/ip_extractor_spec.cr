require "../../spec_helper"

describe Crlocator::Core::GeoLocator do
  it "should extract IP from X_Client_Ip" do
    Crlocator::Core::IPExtractor.detect_ip(SpecHelper.build_headers("X_Client_Ip", ["128.82.1.31"])).should be_truthy
  end

  it "should extract not extract IP from unknown headers" do
    Crlocator::Core::IPExtractor.detect_ip(SpecHelper.build_headers("x_custom_ip", ["128.82.1.31"])).should be_falsey
  end

  it "should return matched IP" do
    Crlocator::Core::IPExtractor.detect_ip(SpecHelper.build_headers("X_Client_Ip", ["128.82.1.31"])).should eq("128.82.1.31")
  end

  it "should return first IP when comma seperated" do
    Crlocator::Core::IPExtractor.detect_ip(SpecHelper.build_headers("X_Client_Ip", ["128.82.1.31, 8.8.8.8"])).should eq("128.82.1.31")
  end
end

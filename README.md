# crlocator

A single binary, fire-n-forget IP to geolocation server written in crystal language. 
IP geolocation is the mapping of an IP address to the geographic location of the internet from the connected device. 
By geographically mapping the IP address, it provides you with location information such as the country, state, city, zip code, latitude/longitude, ISP, area code, and other information.


## Compiling dev binary
Compiling is dead simple; from project directory root do:

```
mkdir -p bin
shards install
crystal build src/main.cr -o bin/crlocator
```

## Compiling static linked prod binary using docker

```
docker run --rm -it -v $PWD:/app -w /app jrei/crystal-alpine crystal build src/main.cr --static --release --progress -o crlocator-static
```

## Running Docker image

By default the image runs crlocator on port 3000, you can simply pull and run docker image by:

```
docker run -d -e "MMDB_KEY=YOUR_KEY" -p 3000:3000 maxpert/crlocator:latest
```

    In order to support auto update a you will need MMDB_KEY to be set. You can acquire a free key via MaxMind's  site. Once provided if DB is missing it will automatically download the database and keep updating it on configured update interval.

## Usage
For command-line binary usage:

```
Usage: colocator [options] (start)
    -p PORT, --port=PORT             Port to start listening on (default: 3000)
    -d PATH, --db=PATH               Path to database (default: ./GeoLite2-City.mmdb)
    -t HOURS, --update=HOURS         Update DB interval hours (default: 24)
    -l PATH, --log=PATH              Path to log fine (default: STDOUT)
    -v, --verbose                    Verbose logging
    -b, --benchmark                  Allow benchmarking via Loader.io
    -h, --help                       Show this help
```

## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [[maxpert]](https://gitlab.com/maxpert) Zohaib - creator, maintainer
